# CossartLabToNWB
A preprocessing pipeline to create NWB file

# **Installation**
```
conda create -n <environment_name> python=3.7
conda activate <environment_name>
conda install -c conda-forge fa2
pip install -r <cossartlab_to_nwb_env.txt>
```

# **Run**
run 'nwb_pre_processing.py'
